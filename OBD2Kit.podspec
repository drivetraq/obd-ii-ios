Pod::Spec.new do |s|

  s.name                    = "OBD2Kit"
  s.version                 = "1.0.0"
  s.summary                 = "An OBD-II and ELM372 compatible library for iOS and OSX."
  # s.description           = <<-DESC
  s.homepage                = "https://bitbucket.org/drivetraq/obd-ii-ios/overview"
  # s.screenshots           = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"
  s.license                 = "Apache"
  s.authors                 = { "Michael Gile", "Trevor Boyer" => "trevor.boyer@drivetraq.com" }
  s.ios.deployment_target   = "5.0"
  s.osx.deployment_target   = "10.7"
  s.source                  = { :git => "https://bitbucket.org/drivetraq/obd-ii-ios.git", :tag => "v1.0.0" }
  s.source_files            = "Classes", "Classes/**/*.{h,m}"
  s.exclude_files           = "Classes/Exclude"

end
